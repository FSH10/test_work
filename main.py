from fastapi import FastAPI

from ImageProcess.api.image_process_router import image_process_router

app = FastAPI(
    title="Car detection app"
)

app.include_router(image_process_router)