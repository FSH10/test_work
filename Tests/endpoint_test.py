from io import BytesIO

from fastapi.testclient import TestClient
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from main import app

client = TestClient(app)


def test_post_main():
    pipeline_id = 1
    client = TestClient(app)
    current_directory = os.path.dirname(__file__)
    image_filename = "image.jpeg"
    image_path = os.path.join(current_directory, image_filename)

    with open(image_path, "rb") as image_file:
        response = client.post(f"/process_image/{pipeline_id}", files={"file": (image_filename, image_file)})

    assert response.status_code == 200
