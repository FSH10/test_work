from sqlalchemy import Column, Integer, Float, String
from database import Base


class ProcessRecords(Base):
    __tablename__ = "process_records"

    id = Column(Integer, primary_key=True, index=True)
    pipeline_id = Column(Integer, index=True)
    top_left_x = Column(Integer)
    top_left_y = Column(Integer)
    w = Column(Integer)
    h = Column(Integer)
    conf = Column(Float)
    label = Column(Integer)