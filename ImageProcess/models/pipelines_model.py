from sqlalchemy import Column, Integer, Float, String
from database import Base


class Pipelines(Base):
    __tablename__ = "pipelines"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
