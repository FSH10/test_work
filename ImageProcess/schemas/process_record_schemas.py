from pydantic import BaseModel
from sqlalchemy import Numeric


class GetRecordScheme(BaseModel):
    id: int
    pipeline_id: int
    top_left_x: int
    top_left_y: int
    w: int
    h: int
    conf: float
    label: int

    class Config:
        arbitrary_types_allowed = True