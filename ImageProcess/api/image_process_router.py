from typing import Optional

from fastapi import UploadFile, File, APIRouter, Depends
import cv2
import numpy as np
import base64

from sqlalchemy.ext.asyncio import AsyncSession

from ImageProcess.crud.crud import crud_manager
from ImageProcess.models.process_record_model import ProcessRecords
from ImageProcess.schemas.process_record_schemas import GetRecordScheme
from ModelExecution.pipeline_1 import pipeline_1
from database import get_async_session

image_process_router = APIRouter(
    tags=["Image Process"]
)


@image_process_router.post("/process_image/{pipeline_id}", response_model=Optional[GetRecordScheme])
async def process_image(pipeline_id: int, file: UploadFile = File(...),
                        session: AsyncSession = Depends(get_async_session)):

    pipelines = {1: pipeline_1}

    image = await file.read()
    np_array = np.frombuffer(image, np.uint8)
    image = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
    resized_image = cv2.resize(image, (640, 640))
    normalized_image = cv2.normalize(resized_image, None, 0, 255, cv2.NORM_MINMAX)
    _, encoded_image = cv2.imencode('.jpg', normalized_image)
    base64_image = base64.b64encode(encoded_image.tobytes()).decode('utf-8')
    process_pipeline = await pipelines[pipeline_id](base64_image)
    await pipelines[pipeline_id](base64_image)

    if process_pipeline is not None:
        process_pipeline["pipeline_id"] = pipeline_id
        return await crud_manager.create_one(process_pipeline, ProcessRecords, session)

    return None
