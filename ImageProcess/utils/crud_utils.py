import time
from functools import wraps


def timing_counter(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        execution_time = round((end_time - start_time) * 100, 5)
        print(f"{execution_time} ms")
        return result

    return wrapper


def catch_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            return {"message": f"Error: {e}"}

    return wrapper