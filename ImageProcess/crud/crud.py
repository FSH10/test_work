from sqlalchemy import select, insert
from sqlalchemy.ext.asyncio import AsyncSession

from ImageProcess.utils.crud_utils import catch_error, timing_counter


class CrudManager:
    @catch_error
    @timing_counter
    async def get_one(self, entity_id: int, model, session: AsyncSession) -> dict:
        query = select(model).where(model.id == entity_id)
        return await self.__execute_query(query, session)

    async def get_all(self):
        pass

    @catch_error
    @timing_counter
    async def create_one(self, entity, model, session: AsyncSession) -> dict:
        query = insert(model).values(**entity).returning(model)
        return await self.__execute_query(query, session)

    async def update_one(self):
        pass

    async def delete_one(self):
        pass

    @staticmethod
    async def __execute_query(query, session: AsyncSession):
        db_result = await session.execute(query)
        await session.commit()
        await session.close()
        return db_result.scalars().one()


crud_manager = CrudManager()
