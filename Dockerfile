FROM python:3.12

RUN apt-get update && apt-get install -y libgl1-mesa-glx

RUN mkdir /fastapi

WORKDIR /fastapi
RUN mkdir -p ./logs

COPY pyproject.toml poetry.lock ./

RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

EXPOSE 8000

COPY . .

CMD alembic upgrade head && gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8000