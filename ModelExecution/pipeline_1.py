import random

from starlette.responses import JSONResponse


def generate_fake_coordinates():
    top_left_x = random.randint(0, 640)
    top_left_y = random.randint(0, 640)
    w = random.randint(0, 640 - top_left_x)
    h = random.randint(0, 640 - top_left_y)
    conf = random.uniform(0, 1.0)
    label = 1
    return {"top_left_x": top_left_x, "top_left_y": top_left_y, "w": w, "h": h, "conf": conf, "label": label}


async def pipeline_1(image_base64: str):
    if random.choice([True, False]):
        return None
    else:
        return generate_fake_coordinates()
